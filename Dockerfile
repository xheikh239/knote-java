#Already pulled image from hub.docker.com
FROM adoptopenjdk/openjdk11

WORKDIR /opt

ENV PORT 8080

EXPOSE 8080

RUN mkdir -p /home/cheikh/Desktop/MS_storage/

COPY target/*.jar /opt/app.jar

ENTRYPOINT exec java $JAVA_OPTS -jar app.jar
